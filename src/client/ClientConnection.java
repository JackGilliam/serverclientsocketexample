package client;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * User: Jack Gilliam
 * Date: 3/14/13
 * Time: 3:09 PM
 */
public class ClientConnection implements Runnable {

    private boolean shutDown = false;
    private static Thread inputThread;
    private static Thread outputThread;

    private static Socket _clientSocket;
    private static boolean _includeOutputStream;

    public ClientConnection(Socket clientSocket) {
        this(clientSocket, true);
    }

    public ClientConnection(Socket clientSocket, boolean includeOutputStream) {
        _clientSocket = clientSocket;
        _includeOutputStream = includeOutputStream;
    }

    public void readFromInputStream() {
        try (InputStream clientInputStream = _clientSocket.getInputStream()) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientInputStream));
            while(isAlive() && !shutDown && !_clientSocket.isInputShutdown()) {
                if(clientInputStream.available() > 0) {
                    String data = bufferedReader.readLine();
                    if(data.equalsIgnoreCase("exit") || data.equalsIgnoreCase("disconnect")) {
                        shutdown();
                    }
                    else {
                        printLine(data);
                    }
                }
            }
        }
        catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        finally {
            System.out.println("Disconnecting InputStream");
        }
    }

    public void writeToOutputStream() {
        Scanner consoleReader = null;
        PrintWriter printWriter = null;
        try {
            consoleReader = new Scanner(System.in);
            printWriter = new PrintWriter(_clientSocket.getOutputStream());
            System.out.println("Type exit to exit");
            String data;
            do {
                data = consoleReader.nextLine();
                if(!_clientSocket.isOutputShutdown()) {
                    printWriter.println(data);
                    printWriter.flush();
                }
            } while(!shutDown && isAlive() && !_clientSocket.isOutputShutdown() && (data == null || !data.toLowerCase().contains("exit")));
        }
        catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        finally {
            if(consoleReader != null) {
                consoleReader.close();
            }
            if(printWriter != null) {
                printWriter.close();
            }
            System.out.println("Disconnecting outputStream");
        }
    }

    public synchronized boolean isAlive() {
        return _clientSocket.isConnected() && !_clientSocket.isClosed();
    }

    @Override
    public void run() {
            inputThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    readFromInputStream();
                }
            });
            inputThread.start();
        if(_includeOutputStream) {
            outputThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    writeToOutputStream();
                }
            });
            outputThread.start();
        }
    }

    private synchronized void printLine(String data) {
        System.out.println(data);
        System.out.flush();
    }

    public void shutdown() {
        shutDown = true;
    }
}
