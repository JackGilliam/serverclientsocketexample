package client;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * User: Jack Gilliam
 * Date: 3/14/13
 * Time: 1:22 PM
 */
public class ChatClient {

    private static final String HOST = "localhost";
    private static final int PORT = 8000;

    public static void main(String[] args) {
        ChatClient chatClient = new ChatClient();
        chatClient.startClient();
    }

    public void startClient() {
        try(final Socket connection = new Socket(HOST, PORT)) {
            ClientConnection clientConnection = new ClientConnection(connection);
            Thread connectionThread = new Thread(clientConnection);
            connectionThread.start();
            while(clientConnection.isAlive()) {
            }
        }
        catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }
}
