package server;

import client.ClientConnection;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * User: Jack Gilliam
 * Date: 3/14/13
 * Time: 1:03 PM
 */
public class ChatServer {

    private static final int PORT = 8000;
    private static final List<ClientConnection> connectedClients = new ArrayList<ClientConnection>();
    private static final int SERVER_TIMEOUT = 10000;
    private static boolean done = false;

    public static void main(String[] args) {
        ChatServer chatServer = new ChatServer();
        chatServer.startServer();
    }

    public void startServer() {
        try (final ServerSocket serverSocket = new ServerSocket(PORT)) {
            serverSocket.setSoTimeout(SERVER_TIMEOUT);
            final Thread checkForTypedThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    checkForTyped();
                }
            });
            checkForTypedThread.start();
            System.out.println("Connected");
            Thread removeDisconnectedThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    removeDisconnectedClients();
                }
            });
            while(!done) {
                try {
                    ClientConnection clientConnection = new ClientConnection(serverSocket.accept(), false);
                    System.out.println("Client Connected");
                    final Thread clientThread = new Thread(clientConnection);
                    clientThread.start();
                    synchronized (connectedClients) {
                        connectedClients.add(clientConnection);
                    }
                    if(!removeDisconnectedThread.isAlive()) {
                        removeDisconnectedThread.start();
                    }
                }
                catch(SocketTimeoutException ste) {
                    if(!done) {
                        done = connectedClients.size() <= 0;
                    }
                    if(done) {
                        synchronized (connectedClients) {
                            for(ClientConnection connected : connectedClients) {
                                connected.shutdown();
                            }
                        }
                        System.out.println("No more clients, shutting down");
                    }
                }
            }
        }
        catch(IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    public void removeDisconnectedClients() {
        try {
            List<ClientConnection> toRemove;
            while(!done) {
                Thread.sleep(5000);
                toRemove = new LinkedList<ClientConnection>();
                synchronized (connectedClients) {
                    for(final ClientConnection clientConnection : connectedClients) {
                        if(!clientConnection.isAlive()) {
                            toRemove.add(clientConnection);
                        }
                    }
                    for(final ClientConnection clientConnection : toRemove) {
                        connectedClients.remove(clientConnection);
                        System.out.println("Client Disconnected");
                    }
                }
            }
        }
        catch (InterruptedException ie) {
            throw new RuntimeException(ie);
        }
    }

    public void checkForTyped() {
        try{
            InputStream inputStream = System.in;
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputStream));
            do {
                if(inputStream.available() > 0) {
                    String data = inputReader.readLine();
                    done = data.equalsIgnoreCase("exit");
                }
            } while(!done);
            inputReader.close();
            inputStream.close();
        }
        catch (Exception ioe) {
            throw new RuntimeException(ioe);
        }
    }
}
